﻿using System;

namespace FindEmailDomain
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(findEmailDomain("example-indeed@strange-example.com"));
        }

        static string findEmailDomain(string address) {
            int lastIndexOf = address.LastIndexOf("@");
            if(lastIndexOf == -1){
                return "";
            }
            return address.Substring(lastIndexOf + 1);     
        }

    }
}
